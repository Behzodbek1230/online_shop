from django import template
from django.utils.safestring import mark_safe

from mainapp.models import Smartphone


register = template.Library()


TABLE_HEAD = """
                <table class="table">
                  <tbody>
             """

TABLE_TAIL = """
                  </tbody>
                </table>
             """

TABLE_CONTENT = """
                    <tr>
                      <td>{name}</td>
                      <td>{value}</td>
                    </tr>
                """

PRODUCT_SPEC = {
    'notebook': {
        'dioganal': 'diagonal',
        'display turi': 'display_type',
        'prossesor chastotasi': 'processor_freq',
        'operativ xotira': 'ram',
        'videokarta': 'video',
        'batareya ishlash vaqti': 'time_without_charge'
    },
    'smartphone': {
        'dioganal': 'diagonal',
        'display turi': 'display_type',
        'ekran ulchami': 'resolution',
        'batareya hajmi': 'accum_volume',
        'operativ xotira': 'ram',
        'SD': 'sd',
        'SD maksimal hajmi': 'sd_volume_max',
        'Кamera (МP)': 'main_cam_mp',
        'Asosiy kamera (МP)': 'frontal_cam_mp'
    }
}


def get_product_spec(product, model_name):
    table_content = ''
    for name, value in PRODUCT_SPEC[model_name].items():
        table_content += TABLE_CONTENT.format(name=name, value=getattr(product, value))
    return table_content


@register.filter
def product_spec(product):
    model_name = product.__class__._meta.model_name
    if isinstance(product, Smartphone):
        if not product.sd:
            PRODUCT_SPEC['smartphone'].pop('SD maksimal hajmi', None)
        else:
            PRODUCT_SPEC['smartphone']['SD maksimal hajmi'] = 'sd_volume_max'
    return mark_safe(TABLE_HEAD + get_product_spec(product, model_name) + TABLE_TAIL)

